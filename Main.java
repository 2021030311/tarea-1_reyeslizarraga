package main;

public class Main {
    public static void main(String[] args) {
        Producto prod = new Producto();
        double preVenta, preCompra, worth;
        
        prod.setCodigoProducto(1203);
        prod.setDescripcion("Atun de agua");
        prod.setUnidadMedida("piezas");
        prod.setPrecioCompra(10.0);
        prod.setPrecioVenta(15.0);
        prod.setCantidadProducto(100);
        preVenta = prod.getPrecioVenta() * prod.getCantidadProducto();
        preCompra = prod.getPrecioCompra() * prod.getCantidadProducto();
        worth = preVenta - preCompra;
        
        System.out.println("Código del producto: " + prod.getCodigoProducto());
        System.out.println("Descripción: " + prod.getDescripcion());
        System.out.println("Unidad de medida: " + prod.getUnidadMedida());
        System.out.println("Precio de compra: " + prod.getPrecioCompra());
        System.out.println("Precio de venta: " + prod.getPrecioVenta());
        System.out.println("Cantidad de producto: " + prod.getCantidadProducto());
        System.out.println("Cálculo de precio de venta: " + preVenta);
        System.out.println("Cálculo de precio de compra: " + preCompra);
        System.out.println("Cálculo de ganancia: " + worth);
    }
}
