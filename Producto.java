package main;

public class Producto {
    private int codigoProducto;
    private String descripcion;
    private String unidadMedida;
    private double precioCompra;
    private double precioVenta;
    private int cantidadProducto;
    
    public Producto() {
    }
    
    public Producto(int codigoProducto, String descripcion, String unidadMedida, double precioCompra, double precioVenta, int cantidadProducto,double calculoPrecioVenta, double calculoPrecioCompra, double calculoGanancia) {
        this.codigoProducto = codigoProducto;
        this.descripcion = descripcion;
        this.unidadMedida = unidadMedida;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidadProducto = cantidadProducto;
    }
    
    public int getCodigoProducto() {
        return codigoProducto;
    }
    
    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getUnidadMedida() {
        return unidadMedida;
    }
    
    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }
    
    public double getPrecioCompra() {
        return precioCompra;
    }
    
    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }
    
    public double getPrecioVenta() {
        return precioVenta;
    }
    
    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }
    
    public int getCantidadProducto() {
        return cantidadProducto;
    }
    
    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }
}
